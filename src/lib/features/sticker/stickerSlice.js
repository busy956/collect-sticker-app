import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    stickerList: [
        { name: '쌀국수', imgName: '1.jpg', isPick: false },
        { name: '칼국수', imgName: '2.jpg', isPick: false },
        { name: '돈까스', imgName: '3.jpg', isPick: false },
        { name: '햄버거', imgName: '4.jpg', isPick: false },
        { name: '국밥', imgName: '5.jpg', isPick: false },
        { name: '덮밥', imgName: '6.jpg', isPick: false },
        { name: '분식', imgName: '7.jpg', isPick: false },
        { name: '초밥', imgName: '7.jpg', isPick: false },
        { name: '파스타', imgName: '8.jpg', isPick: false },
        { name: '한식뷔페', imgName: '9.jpg', isPick: false },
        { name: '중식뷔페', imgName: '10.jpg', isPick: false },
        { name: '토스트', imgName: '11.jpg', isPick: false },
        { name: '마라탕', imgName: '12.jpg', isPick: false },
        { name: '카레', imgName: '13.jpg', isPick: false },
        { name: '배달', imgName: '14.jpg', isPick: false },
        { name: '제육볶음', imgName: '15.jpg', isPick: false },
        { name: '편의점', imgName: '16.jpg', isPick: false },
    ]
}

const stickerSlice = createSlice({
    name: 'sticker',
    initialState,
    reducers: {
        // 0 ~ stickerList.length
        // outBoundRange에러 (범위를 벗어났다)
        pickSticker: (state) => {
            const randomIndex = Math.floor(Math.random() * state.stickerList.length)
            state.stickerList[randomIndex].isPick = true
        }
    },
})

export const {pickSticker} = stickerSlice.actions

export default stickerSlice.reducer