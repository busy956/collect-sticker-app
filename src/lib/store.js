import { configureStore } from '@reduxjs/toolkit'
import stickerReducer from "@/lib/features/sticker/stickerSlice";

export default configureStore({
    reducer: {
        sticker: stickerReducer,
    },
})