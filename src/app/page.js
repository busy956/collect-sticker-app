'use client'
import {useSelector, useDispatch} from "react-redux";
import {pickSticker} from "@/lib/features/sticker/stickerSlice";


export default function Home() {
    const {stickerList} = useSelector((state) => state.sticker);
    const dispatch = useDispatch();

    const handleEatBread = () => {
        dispatch(pickSticker())
    }

    return (
        <main className='flex flex-col items-center'>
            <div className='text-2xl mt-10'>
                스티커를 뽑아볼까요 ~ ?
            </div>
            <div>
                <button onClick={handleEatBread} className='border-2 py-1.5 px-3 mt-10'>빵먹기</button>
            </div>
            <div className='flex-wrap items-center justify-between w-auto mt-10'>
                <ul>
                    {stickerList.map((sticker, index) => (
                        <li key={index} className='border-2 w-40 text-center'>{sticker.isPick ? sticker.name : '?'}</li>
                    ))}
                </ul>
            </div>
        </main>
    );
}
